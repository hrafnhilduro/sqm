A project in the works in the course Software Quality Management at HÍ univercity in Iceland  
  
  Right click on the SQM folder and go to Run as...  
    Then Select Maven Build  
  If it asks for it, write COMPILE as the goal.  
  
The project supports automated testing.
    Right click on the project and select Run as... Maven Test
    That will build the project and run test until it runs into an error.
    
Lets take a look at how the CI Pipelines are working
    
    Try one, it manages to build but not to run the tests
![Pipeline Status](Badges/Badge1.JPG)]

    Try two, adding code coverage
![Pipeline Status](Badges/Badge2.JPG)]

    Added Sonar Cloud to the pipeline but it will not work
![Sonar Cloud Status](Badges/Badge3.JPG)